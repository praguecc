##############################################################################
#                              GRID SETUP                                    #
##############################################################################
ActiveThorns = "CoordBase"

CoordBase::domainsize = "minmax"
CoordBase::xmin = 0
CoordBase::xmax = 128
CoordBase::ymin = 0
CoordBase::ymax = 0
CoordBase::zmin = 0
CoordBase::zmax = 128

CoordBase::spacing = "numcells"
CoordBase::ncells_x = 128
CoordBase::ncells_y = 1
CoordBase::ncells_z = 128

CoordBase::boundary_size_x_lower     = 5
CoordBase::boundary_size_y_lower     = 0
CoordBase::boundary_size_z_lower     = 5
CoordBase::boundary_size_x_upper     = 5
CoordBase::boundary_size_y_upper     = 0
CoordBase::boundary_size_z_upper     = 5

CoordBase::boundary_shiftout_x_lower = 1
CoordBase::boundary_shiftout_y_lower = 0
CoordBase::boundary_shiftout_z_lower = 1

CoordBase::boundary_internal_y_lower = 1
CoordBase::boundary_internal_y_upper = 1

ActiveThorns = "CartGrid3d"

CartGrid3d::type                 = "coordbase"

ActiveThorns = "Carpet CarpetLib CarpetRegrid2 SphericalSurface IOUtil InitBase"

Carpet::verbose                  = 0
Carpet::domain_from_coordbase    = 1
Carpet::init_fill_timelevels     = 1
Carpet::poison_new_timelevels    = 0
Carpet::max_refinement_levels    = 15
Carpet::refinement_factor        = 2       # anything else not implemented
Carpet::refinement_centering     = "vertex"
#Carpet::ghost_size               = 3
Carpet::ghost_size_x             = 5
Carpet::ghost_size_y             = 0
Carpet::ghost_size_z             = 5
Carpet::use_buffer_zones         = 1
Carpet::prolongation_order_space = 9
Carpet::prolongation_order_time  = 1
Carpet::num_threads              = -1
Carpet::use_tapered_grids        = 1
CarpetLib::poison_new_memory     = 0
CarpetLib::print_timestats_every = 1024

CarpetRegrid2::verbose                 = 1
CarpetRegrid2::regrid_every            = 0
CarpetRegrid2::num_centres             = 1
CarpetRegrid2::movement_threshold_1    = 0.1
CarpetRegrid2::position_x_1            = 0
CarpetRegrid2::num_levels_1            = 12
CarpetRegrid2::radius_1[1]             = 64
CarpetRegrid2::radius_1[2]             = 56
CarpetRegrid2::radius_1[3]             = 48
CarpetRegrid2::radius_1[4]             = 42
CarpetRegrid2::radius_1[5]             = 24
CarpetRegrid2::radius_1[6]             = 12
CarpetRegrid2::radius_1[7]             = 4
CarpetRegrid2::radius_1[8]             = 2
CarpetRegrid2::radius_1[9]             = 1.25
CarpetRegrid2::radius_1[10]            = 0.75
CarpetRegrid2::radius_1[11]            = 0.375
CarpetRegrid2::add_levels_automatically = 0

ActiveThorns = "ReflectionSymmetry CarpetInterp AEILocalInterp"

ReflectionSymmetry::reflection_z   = 1
ReflectionSymmetry::avoid_origin_z = 0
ReflectionSymmetry::reflection_y   = 0
ReflectionSymmetry::avoid_origin_y = 0
ReflectionSymmetry::reflection_x   = 1
ReflectionSymmetry::avoid_origin_x = 0



##############################################################################
#                            INITIAL DATA                                    #
##############################################################################

ActiveThorns = "ADMBase ADMCoupling ADMMacros TeukolskyData"

ADMBase::metric_timelevels        = 2
ADMBase::initial_data             = "teukolskydata"
ADMBase::initial_lapse            = "teukolsky_max"
ADMBase::initial_dtlapse          = "zero"
ADMBase::initial_shift            = "zero"
ADMBase::initial_dtshift          = "zero"

ADMMacros::spatial_order = 4

TeukolskyData::family = 0
TeukolskyData::Amplitude = -1.2243164
TeukolskyData::basis_order_0 = 160
TeukolskyData::basis_order_1 = 16
TeukolskyData::scale_factor = 3.0
TeukolskyData::solution_branch = 1
TeukolskyData::max_iter = 32

##############################################################################
#                             EVOLUTION                                      #
##############################################################################

Cactus::terminate       = "time"
Cactus::cctk_final_time = 30

ActiveThorns = "MoL Time"

MoL::initial_data_is_crap   = 0
MoL::ODE_Method             = "RK4"
MoL::MoL_Intermediate_Steps = 4
MoL::MoL_Num_Scratch_Levels = 1
MoL::MoL_NaN_Check          = 0

Time::dtfac = 0.125

ActiveThorns = "ML_BSSN ML_BSSN_Helper CoordGauge GenericFD NewRad Boundary StaticConformal TmunuBase
                SymBase LoopControl QuasiMaximalSlicingMG"

ADMBase::evolution_method         = "ML_BSSN"
ADMBase::lapse_evolution_method   = "ML_BSSN"
ADMBase::dtlapse_evolution_method = "ML_BSSN"
ADMBase::shift_evolution_method   = "ML_BSSN"
ADMBase::dtshift_evolution_method = "ML_BSSN"

ML_BSSN::verbose                       = 1
ML_BSSN::timelevels                    = 2
ML_BSSN::fdOrder                       = 8
ML_BSSN::harmonicF                     = 2      # \dot{α} = - f α^n K
ML_BSSN::harmonicN                     = 1      # 1+log
ML_BSSN::conformalMethod               = 1      # χ method
ML_BSSN::ShiftGammaCoeff               = 0
ML_BSSN::HarmonicShift                 = 0
ML_BSSN::LapseACoeff                   = 0.0
ML_BSSN::ShiftBCoeff                   = 0.0
ML_BSSN::AlphaDriver                   = 0.0
ML_BSSN::BetaDriver                    = 20
ML_BSSN::LapseAdvectionCoeff           = 0.0
ML_BSSN::ShiftAdvectionCoeff           = 0.0
ML_BSSN::ML_BSSN_Advect_calc_offset    = 1  # disable calling the advect function
ML_BSSN::ML_BSSN_convertToADMBase_calc_offset = 1
ML_BSSN::MinimumLapse                  = 1e-08
ML_BSSN::dt_lapse_shift_method         = "correct"
ML_BSSN::EpsDiss                       = 0
#ML_BSSN::my_initial_boundary_condition = "extrapolate-gammas"
ML_BSSN::my_initial_boundary_condition = "none"
ML_BSSN::my_rhs_boundary_condition     = "none"
Boundary::radpower                     = 2
ML_BSSN::ML_log_confac_bound           = "none"
ML_BSSN::ML_metric_bound               = "none"
ML_BSSN::ML_Gamma_bound                = "none"
ML_BSSN::ML_trace_curv_bound           = "none"
ML_BSSN::ML_curv_bound                 = "none"
#ML_BSSN::ML_lapse_bound                = "none"
#ML_BSSN::ML_dtlapse_bound              = "none"
ML_BSSN::ML_shift_bound                = "none"
ML_BSSN::ML_dtshift_bound              = "none"
ML_BSSN::KEvolFactor                   = 1
ML_BSSN::WFactor                       = 1.0
ML_BSSN::lapse_source                  = "QMS_MG"

QuasiMaximalSlicingMG::fd_stencil = 2
QuasiMaximalSlicingMG::loglevel = "info"
QuasiMaximalSlicingMG::stats_every = 128
QuasiMaximalSlicingMG::tol_residual_base = 1e-8
QuasiMaximalSlicingMG::maxiter = 4096
QuasiMaximalSlicingMG::cfl_factor = 0.17
QuasiMaximalSlicingMG::nb_relax_post = 24
QuasiMaximalSlicingMG::switchoff_time = 30
QuasiMaximalSlicingMG::solve_level    = 9
QuasiMaximalSlicingMG::solve_level_max = 6
QuasiMaximalSlicingMG::boundary_offset = 1
QuasiMaximalSlicingMG::exact_size = 17


ActiveThorns = "Dissipation SpaceMask"

Dissipation::order = 9
Dissipation::vars  = "
        ML_BSSN::ML_log_confac
        ML_BSSN::ML_metric
        ML_BSSN::ML_trace_curv
        ML_BSSN::ML_curv
        ML_BSSN::ML_Gamma
        ML_BSSN::ML_lapse
"

ActiveThorns = "CarpetInterp AEILocalInterp"

ActiveThorns = "ML_Kretschmann ML_Kretschmann_Helper"

ActiveThorns = "NullSurf"

NullSurf::solve_level = 4
NullSurf::photons_per_surface = 256
NullSurf::nb_surfaces = 32
NullSurf::launch_delta = 0.125

###############################################################################
#                               ANALYSIS                                      #
###############################################################################

ActiveThorns = "CarpetReduce"

###############################################################################
#                             OUTPUT                                          #
###############################################################################

ActiveThorns = "TimerReport"

TimerReport::out_every = 131072
TimerReport::n_top_timers = 30

ActiveThorns = "CarpetIOBasic CarpetIOHDF5"

IO::out_dir       = $parfile
IO::out_criterion = "time"
IO::out_dt        = 0.5
IO::out_every     = 1
IO::out_mode      = "onefile"
IO::out_unchunked = 1
IOHDF5::checkpoint = 1
IO::checkpoint_every = 32768
IO::checkpoint_on_terminate = 1
IO::checkpoint_dir = $parfile
IO::recover = "autoprobe"
IO::recover_dir = $parfile

IOBasic::outInfo_criterion  = "iteration"
IOBasic::outInfo_every      = 2048
IOBasic::outInfo_reductions = "minimum maximum average norm1 norm2"
IOBasic::outInfo_vars       = "
        ML_BSSN::H
        ML_BSSN::trK
        ML_BSSN::alpha
        "

IOHDF5::compression_level      = 0
IOHDF5::output_symmetry_points = 1
IOHDF5::out3D_ghosts        = 1
IOHDF5::out3D_outer_ghosts  = 1
IOHDF5::out_vars           = "
ML_BSSN::phi
ML_BSSN::gt11
ML_BSSN::gt22
ML_BSSN::gt33
ML_BSSN::gt13
ML_BSSN::At11
ML_BSSN::At22
ML_BSSN::At33
ML_BSSN::At13
ML_BSSN::trK
ML_BSSN::ML_Ham
ML_BSSN::ML_mom
ML_BSSN::ML_Gamma
ML_BSSN::ML_lapse
ML_BSSN::term1
ML_BSSN::term2
ML_Kretschmann::ML_Kretschmann
ML_Kretschmann::ML_zeta
ML_BSSN::W
NullSurf::pos_x
NullSurf::pos_z
NullSurf::mom_x
NullSurf::mom_x
"
#QuasiMaximalSlicingMG::W_val0
#QuasiMaximalSlicingMG::W_val1
#QuasiMaximalSlicingMG::W_pred0
#QuasiMaximalSlicingMG::W_pred1

IOHDF5::out1D_vars = "
ADMBASE::lapse
ML_BSSN::ML_metric
ML_BSSN::ML_curv
ML_BSSN::ML_trace_curv
ML_BSSN::ML_log_confac
ML_BSSN::ML_Ham
ML_BSSN::ML_mom
ML_BSSN::ML_Gamma
ML_BSSN::ML_lapse
ML_BSSN::term1
ML_BSSN::term2
ML_BSSN::W
ML_Kretschmann::ML_Kretschmann
ML_Kretschmann::ML_zeta
NullSurf::pos_x
NullSurf::pos_z
NullSurf::mom_x
NullSurf::mom_x
"
IOHDF5::out1D_criterion = "time"
IOHDF5::out1D_dt = 0.015625

ActiveThorns = "CarpetIOScalar"
IOScalar::outScalar_criterion = "iteration"
IOScalar::outScalar_every = 1
IOScalar::outScalar_reductions = "minimum maximum"
IOScalar::outScalar_vars = "
ML_Kretschmann::ML_Kretschmann
ML_Kretschmann::ML_zeta
ML_BSSN::ML_trace_curv
"
